#!/usr/bin/env nextflow

nextflow.enable.dsl=2

process INIT_WORKSPACE {
	output:
	path '.meta', emit: meta

	shell:
	'''
	philosopher workspace --nocheck --analytics false --init
	'''
}

