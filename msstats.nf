#!/usr/bin/env nextflow

nextflow.enable.dsl=2

process MSSTATS {
	publishDir params.output_dir, mode: 'copy'
    
	input:
    path downstream_dir

    output:
    path 'msstats/', emit: msstats_out

	shell:
	'''
	msstats "!{downstream_dir}/MSstats.csv" labelfree msstats
	'''
}

