#!/usr/bin/env nextflow

nextflow.enable.dsl=2

process BUILD_FASTA {
	debug true
	stageInMode "copy"

	input:
	path meta
	val id
	path custom_fasta

	output:
	path '*.fas', emit: db
	path '.meta', emit: meta

	shell:
	'''
	ppp --path "$PWD" "!{meta}/meta.bin"

	philosopher database --id !{id} --contam --reviewed
	
	DOWNLOAD="$(find . -maxdepth 1 -name '*.fas' -not -name '!{custom_fasta}')"
	cat !{custom_fasta} >> $DOWNLOAD
	
	philosopher database --custom $DOWNLOAD --contam
	rm $DOWNLOAD
	'''
}
