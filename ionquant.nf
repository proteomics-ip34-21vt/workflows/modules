#!/usr/bin/env nextflow

nextflow.enable.dsl=2

process IONQUANT {
	stageInMode "copy"
	publishDir params.output_dir, mode: 'copy'

	input:
	path report_dirs
	path spec_dirs
	path pepXMLs

	output:
	path 'ionquant/', emit: output_dir

	shell:
	'''
	ARGS=""

    for spec_dir in !{spec_dirs}
	do
      	ARGS="$ARGS --specdir $spec_dir" 
	done

	GROUP_NAMES="$(find . -name "psm.tsv" | xargs dirname)"
    for group_dir in $GROUP_NAMES
	do
		GROUP="$(basename $group_dir)"
		ARGS="$ARGS --psm $GROUP/psm.tsv"
	done
	mkdir ionquant 
	ionquant "$ARGS" --mbr 1  --multidir ionquant ./*.pepXML
	'''
}
